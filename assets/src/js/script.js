var content = null;
var image = document.getElementsByClassName("thumbnail");

//include footer and header
fetch("./header.html")
  .then(function(response) {
    return response.text();
  })
  .then(function(data) {
    document.querySelector("header").innerHTML = data;
    //include header then create hamburger menu
    content = document.getElementById("menu");
    document.getElementById("hamburger-menu").innerHTML = content.innerHTML;
  });
fetch("./footer.html")
  .then(function(response) {
    return response.text();
  })
  .then(function(data) {
    document.querySelector("footer").innerHTML = data;
  });


function menu() {
  var menu = document.getElementById("hamburger-menu").parentNode;

  if (menu.style.display == "block") {
    menu.style.display = "none";
  } else {
    menu.style.display = "block";
  }
}
//products
function tabs(evt, product) {
  var i = null;
  var products = null;
  var tablinks = null;
  products = document.getElementsByClassName("product-container");
  for (i = 0; i < products.length; i++) {
    products[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("active");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", " ");
  }
  document.getElementById(product).style.display = "flex";
  evt.currentTarget.className += " active";
}

document.getElementById("openClass1").click();

//parallax on drone
new simpleParallax(image, {
  overflow: true,
  scale: 1.7,
  delay: 0.4
});


//explications techniques
function dynamicValue() {
  var question = document.getElementById("choix-aides").value;
  var questionListe = [
    "Quelle est l'altitude maximale de vos drones ?",
    "Quelle est la composition d'un drone ?",
    "Quelle est la vitesse maximale de vos drones ?",
    "Quelle est la capacité d'emport de vos drones ?" ,
    "Quels sont les types de services que proposent vos drones ?",
    "Quelles sont les règles de sécurité ?",
    "Quel est le manuel d'emploi ?",
    "Comment utiliser l'application mobile ?",
    "Quelle est la durée de vos batteries ?",
    "Comment faire en cas de mauvais temps ?",
    "Comment apprendre  à piloter son drone ?"
  ];
  var reponseListe = [
    "L'altitude maximale est de 150m en France selon la réglementation.",
    "Un drone est composé d’un châssis, d’un système de propulsion et d’un contrôleur de vol.",
    "La vitesse maximale est de 80km/h au compteur.",
    "Nos drones sont capables de supporter jusqu’à 5kg de chargement.",
    "En service à la personne, nous vous proposons la livraison de colis, ramener votre enfant sur le chemin de l’école, promener votre chien… </br> Pour plus d’informations, veuillez vous référer à notre page Services.",
    "Il faut impérativement prendre connaissance de la réglementation en vigueur (lien vers https://www.amateursdedrones.fr/reglementation-legislation-drones/) en France. </br>Les règles de sécurité sont : </br> Interdit de faire voler un drone en zone urbaine (parcs, rues, stades, plages…) utilisez aip-drones.fr pour en savoir plus.</br> Il est autorisé de voler que dans une propriété privée. </br> Vous avez le droit de voler jusqu’à 30 minutes après le coucher du soleil, sauf si vous êtes chez vous.</br> Hauteur maximale de 150 mètres.</br> Ne filmez jamais les gens à leur insu.</br> Ne faites pas voler l’appareil depuis un véhicule en mouvement.</br> Ne perdez jamais de vue votre drone.</br> Les drones utilisés à des fins professionnelles sont soumis à une réglementation plus stricte, avec obligation de posséder le brevet théorique de pilote ULM.</br> Vous risquez 75 000 euros d’amende pour 1 an de prison si vous faites une prise de vue illégale d’une zone sensible (centrales nucléaires, bâtiments militaires et médicaux…)</br> Tous les drones de 800 grammes devront être déclarés via enregistrement électronique et équipés de dispositifs sonores et lumineux.",
    "Le manuel d’emploi est livré avec le drone.",
    "L’application mobile est disponible sur iOs et android.</br> Pour la télécharger, veuillez vous référer à notre page d’Accueil.",
    "Vérifiez toujours que votre appareil a assez de batterie avant de l’utiliser. L’autonomie de la batterie est de 8h.",
    "N’utilisez pas votre drone si les conditions météorologiques ne s’y prêtent pas. Assurez-vous qu’il ne pleut pas et qu’il n’y a pas de vent.",
    "Pour piloter un drone, il faut : </br> Connaître les règles </br> Choisir un drone de petite taille pour commencer</br> Lisez attentivement le manuel d’emploi </br>Entraînez-vous dans une zone dégagée, avec peu ou pas de présence humaine.</br> Commencez par des manœuvres simples. </br> Vérifiez constamment le niveau de batterie indiqué sur l’application mobile, ainsi que la batterie de votre téléphone ou tablette. </br>Prenez connaissance des conditions météorologiques avant de voler.</br> Il vous est recommandé de voler en mode automatique. </br> Comprendre le fonctionnement de la fonction automatisé du « retour à domicile »  </br> Fonctionnement de l’application mobile : le joystick gauche ajuste l’altitude et fait tourner le drone, le joystick droit sert à avancer, reculer, aller à gauche ou à droite.  </br> Entraînement conseillé : Comme dans l’apprentissage de n’importe quelle discipline, l’entraînement régulier est un pilier du pilotage de votre drone. Suivez ces étapes pour vous entraîner à manœuvrer votre drone dans toutes les positions : Appareil face à vous, orienté à droite et à gauche. </br> Positionnement : Après avoir prévu une aire large et sans obstacles pour éviter tout risque de collision, positionnez votre drone, arrière en face de vous, à plat sur une surface horizontale (solide de préférence�� pour prévenir les décollages de travers). La machine est ainsi dans votre référentiel par rapport à l’espace environnant. Décollez donc en douceur à l’aide de la manette gauche en réalisant un mouvement de bas en haut pour pousser les gaz et arracher le drone du sol. Durant cette manœuvre, évitez tout mouvement brusque car certains appareils sont imprévisibles.</br> Stabilisation : Stabiliser votre drone à un mètre du sol en ramenant la manette directionnelle au centre. Atterrissage : Pour atterrir, repérez un point et réalisez le mouvement inverse, c’est- à-dire de haut en bas. Ce mouvement va permettre de ralentir les moteurs et de descendre le drone. Répétez ce mouvement plusieurs fois tout en montant progressivement votre drone. Ne dépassez pas les 10 mètres dans un premier temps. Répétez ces manœuvres en changeant la position de la machine par rapport à la vôtre !"
  ];

  if (question) {
    var qst = questionListe.indexOf(question);
    document.getElementById("dynamique-text").innerHTML = reponseListe[qst];
  }
}
